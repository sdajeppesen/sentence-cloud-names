package com.sda.jeppesen.cloud.sentence.names.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class NameController {

    //jeppesen.cloud.names = kwiatek,biurko,krzesło,nagroda,szafka,but
    @Value("${jeppesen.cloud.names}")
    private String[] names;

    @GetMapping("/name")
    public String getName(){
        // wylosuj liczbę z zakresu do ilości dostępnych wartości
        // zwróć słowo z podanego indeksu
        return names[new Random().nextInt(names.length)];
    }
}
