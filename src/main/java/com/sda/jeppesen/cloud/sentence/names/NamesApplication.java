package com.sda.jeppesen.cloud.sentence.names;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class NamesApplication {

	public static void main(String[] args) {
		SpringApplication.run(NamesApplication.class, args);
	}

}
